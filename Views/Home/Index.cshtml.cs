﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace KCC_App.Views.Home
{
    public class IndexModel : PageModel
    {
        public IActionResult OnGet()
        {
            return new RedirectResult("/Startseite");
        }

    }
}