﻿using Core.Flash;
using Core.Flash.Model;
using DocumentFormat.OpenXml.Wordprocessing;
using KCC_App.Data;
using KCC_App.Helpers;
using KCC_App.KCCModels;
using KCC_App.Models;
using KCC_App.Services;
using KCC_App.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Crypto.Tls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;
using System.Globalization;
using static Org.BouncyCastle.Math.EC.ECCurve;
using NPOI.SS.Formula.Functions;
using KCC_App.Interfaces;
using Hangfire;
using DocumentFormat.OpenXml.InkML;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra.Factorization;
using DocumentFormat.OpenXml.Spreadsheet;

namespace KCC_App.Controllers
{
    //[Route("Batch")]
    [Authentication]
    public class BatchController : ControllerBase
    {
        private readonly ILogger<SettingController> _logger;
        private IHostingEnvironment Environment;
        private object ddlYears;
        readonly KCCConfig cCConfig;
        private readonly IFlasher f;
        readonly ApplicationDbContext dbContext;
        private readonly IConfiguration _configuration;
        private readonly IServiceRepository serviceRepository;
        private readonly IBackgroundJobs backgroundJobs;


        public BatchController(ILogger<SettingController> logger,
            IHostingEnvironment _environment,
            IConfiguration configuration,
            ApplicationDbContext context,
            IFlasher f,
            IServiceRepository serviceRepository,
            IBackgroundJobs backgroundJobs)
        {
            _logger = logger;
            Environment = _environment;
            this._configuration = configuration;
            this.dbContext = context;
            cCConfig = dbContext.KCCConfig.FirstOrDefault();//  Common.GetKCCConfig(this._configuration);
            this.f = f;
            this.serviceRepository = serviceRepository;
            this.backgroundJobs = backgroundJobs;
            //this.cCConfig = context.KCCConfig.AsNoTracking().Include(x => x.EncryptKey).Include(x => x.Mapping).FirstOrDefault();
        }
        public IActionResult List(int status, int? page = 1, string qry = "")
        {
            ViewData["CurrentFilter"] = qry;
            if (page != null && page < 1)
            {
                page = 1;
            }

            var pageSize = 10;
            var batches = this.dbContext.Batches.Include(x => x.BatchResponse).Include(x => x.BatchResponseData)
                .Include("BatchResponseData.BatchStatus");
            if (!string.IsNullOrEmpty(qry))
            {
                batches = batches.Where(x => x.batchId.ToLower().Contains(qry.ToLower()) || x.BatchResponseData.batchAckId.ToLower().Contains(qry.ToLower())
                    || x.financialYear.ToLower().Contains(qry.ToLower()) || x.BatchResponseData.BatchStatus.Name.ToLower().Contains(qry.ToLower()));

            }
            var finalData = batches.Where(ap => ap.BatchResponseData.status == status || status == 0)
                    .Where(x => x.financialYear == HttpContext.Session.GetString(Models.Constants.KEY_FINANCIALYEAR)).OrderByDescending(x => x.dateTime)
            .ToPagedList(page ?? 1, pageSize);
            return View(finalData);
        }
        public IActionResult Detail(string id)
        {

            var model = this.dbContext.Batches
                    .Include(x => x.BatchResponseData)
                    .Include(x => x.applications)

                    .Include("BatchResponseData.BatchStatus")
                    .Include("applications.basicDetails")
                    .Include("applications.accountDetails")
                    .Include("applications.loanDetails")
                    .Include("applications.AppStatus")
                    .FirstOrDefault(x => x.batchId == id);
            return View(model);

        }

        [HttpGet]
        public async Task<IActionResult> RefreshBatches(int? page = 1)
        {
            //BackgroundJobs backgroundJobs = new BackgroundJobs(this._configuration, this.dbContext);
            string jobId=  BackgroundJob.Enqueue<BackgroundJobs>(x => x.RefreshBatchStatus());
            return RedirectToAction("List", new { page = page });
        }

        [HttpGet]
        public async Task<IActionResult> RefreshBatchStatus(string batchAckId, string batchId)
        {
            //BackgroundJobs backgroundJobs = new BackgroundJobs(this._configuration, this.dbContext);
            await backgroundJobs.RefreshBatchStatus(batchAckId, batchId);

            return RedirectToAction("Detail", new { id = batchId });
        }
        public IActionResult Applications(int status = 0, int? page = 1, string qry = "")
        {
          
            ViewData["CurrentFilter"] = qry;
            if (page != null && page < 1)
            {
                page = 1;
            }

            var pageSize = 10;

            var model = this.dbContext.Applications.Include(x => x.accountDetails).Include(x => x.basicDetails).Include(x => x.loanDetails)
                .Include(x => x.AppStatus).Where(ap => ap.AppStatus.status == status || status == 0);

            if (!string.IsNullOrEmpty(qry))
            {
                model = model.Where(ap => ap.uniqueId.ToLower().Contains(qry.ToLower()) || ap.accountDetails.accountNumber.ToLower().Contains(qry.ToLower()) ||
                ap.basicDetails.beneficiaryName.ToLower().Contains(qry.ToLower()) || ap.basicDetails.mobile.ToLower().Contains(qry.ToLower()));

            }
            var app = model.OrderByDescending(x => x.uniqueId).ToPagedList(page ?? 1, pageSize);
            return View(app);
        }
        public IActionResult Application(string id)
        {
            var model = this.dbContext.Applications

                .Include(x => x.accountDetails)
                .Include(x => x.basicDetails)
                .Include(x => x.residentialDetails)
                .Include(x => x.loanDetails)
                .Include(x => x.activities)
                .Include(x => x.AppStatus)
                .Include("activities.activityRows")
                .FirstOrDefault(x => x.uniqueId == id);
            return View(model);
        }
        public IActionResult DeleteMultiple(string batchId,List<string> rows)
        {
            foreach (var rowId in rows)
            {
                var row = dbContext.Applications.Find(rowId);
                if (row != null)
                {
                    row.isDeleted = 1;
                    dbContext.SaveChanges();
                }
            }
            return RedirectToAction("Detail", new {id = batchId});
        }
        public IActionResult DeleteMultipleApplications(string batchId, List<string> rows)
        {
            foreach (var rowId in rows)
            {
                var row = dbContext.Applications.Find(rowId);
                if (row != null)
                {
                    row.isDeleted = 1;
                    dbContext.SaveChanges();
                }
            }
            return RedirectToAction("Applications");
        }
        public async Task<IActionResult> DeleteApplicationBatch(string id,string batchId,string batchAckId)
        {
            var model = this.dbContext.Applications;
            var row = model.FirstOrDefault(m => m.uniqueId == id);
            ViewBag.data = id;

            //Call the api for deletion and if success then delete from local
            //BackgroundJobs backgroundJobs = new BackgroundJobs(this._configuration, this.dbContext);
            try
            {
                await serviceRepository.DeleteApplication(new List<string>(new[]{id}), batchId, batchAckId);
                f.Success("Application Deleted successfully");
            }
            catch (Exception ex)
            {
                f.Danger(ex.Message);
            }
            ////dbContext.SaveChanges();
            return RedirectToAction("Detail",new {id=batchId});
        }
        public async Task<IActionResult> DeleteApplicationList(string id,string batchId,string batchAckId)
        {
            var model = this.dbContext.Applications;
            var row = model.FirstOrDefault(m => m.uniqueId == id);

            ViewBag.data = id;

            //Call the api for deletion and if success then delete from local
            //BackgroundJobs backgroundJobs = new BackgroundJobs(this._configuration, this.dbContext);
            try
            {
                await serviceRepository.DeleteApplication(new List<string>(new[]{id}), batchId,batchAckId);
                f.Success("Application Deleted successfully");
            }
            catch (Exception ex)
            {
                f.Danger(ex.Message);
            }
            ////dbContext.SaveChanges();
            return RedirectToAction("Applications");
        }


        [HttpPost]
        public async Task<IActionResult> UploadBatch(IFormFile postedFile)
        {
            if (postedFile != null)
            {
                string path = Path.Combine(this.Environment.WebRootPath, "Uploads");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string batchId = Common.BatchId(cCConfig.BankCode, dbContext);
                string fileName = batchId + ".xls";// Path.GetFileName(postedFile.FileName);

                string filePath = Path.Combine(path, fileName);
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }

                try
                {
                    DataTable dataTable = Common.ExcelToDataTable(dbContext,filePath, 0);
                    //ServiceRepository serviceRepository = new ServiceRepository(_configuration, dbContext);
                    Batch ret = await serviceRepository.FileUpload(new UploadModel
                    {
                        dt = dataTable,
                        batchId = batchId,
                        filePath = filePath

                    });
                    if (!string.IsNullOrEmpty(ret.BatchResponse.error))
                    {
                        //SetFlash(Helpers.FlashMessageType.Error, ret.error);
                        f.Flash(Types.Danger, ret.BatchResponse.error, dismissable: false);
                    }
                    else
                    {
                        //SetFlash(Helpers.FlashMessageType.Success, String.Format("Batch uploaded successfully with AcknowledmentId {0}", ret.batchId));
                        f.Flash(Types.Success, String.Format("Batch uploaded successfully with AcknowledmentId {0}", ret.BatchResponseData.batchAckId), dismissable: true);
                    }
                    return RedirectToAction("List");

                }
                catch (Exception ex)
                {
                    
                    f.Flash(Types.Danger, /*ex.Message*/ "Incorrect Format", dismissable: false);
                }

            }
            return RedirectToAction("List");
        }
    }

}

