﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.Models
{
    public class Constants
    {
        public const string BATCHID_FORMAT = "{0}{1}{2}";//<6 character DDMMYY>+<3 character Bank code as per NCIP>+< 5 digit running number>
        public const string APPLICATIONID_FORMAT = "{0}{1}";//<14 character BATCH Number>+< 5 digit running number>

        public const string KEY_FINANCIALYEAR = "financialYear";
        public const string KEY_STATECODE = "stateCode";
        public const string KEY_BANKCODE = "bankCode";
        public const string KEY_USERNAME = "Username";

        public const string API_UPLOAD_BATCH = "/submitbatch";
        public const string API_ROUTE_STATE = "/states";
        public const string API_ROUTE_BRANCH = "/branches";
        public const string API_ROUTE_DISTRICT = "/districts";
        public const string API_ROUTE_LOCATION = "/locations";
        public const string API_ROUTE_CONFIG = "/masterConfigs";
        public const string API_ROUTE_CROP = "/crops";
        public const string API_ROUTE_DELETE_APPS = "/deletebyapplicationnumbers";
        public const string API_ROUTE_BATCHSTATUS = "/batchstatus";
        public const string API_ROUTE_BATCHAPPLICATIONSTATUS = "/databybatchackid";
    }
   
}
