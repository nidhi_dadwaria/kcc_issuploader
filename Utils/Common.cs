﻿using DocumentFormat.OpenXml.Drawing;
using KCC_App.Data;
using KCC_App.KCCModels;
using KCC_App.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace KCC_App.Utils
{
    public class Common
    {
        //readonly KCCConfig cCConfig;
        public static string KCCDate(string dateString)
        {
            return Convert.ToDateTime(dateString).ToString("yyyy-MM-dd");
        }
        public static string Decrypt(byte[] cipherText, byte[] Key, byte[] IV)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            string plaintext = null;
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;

                aesAlg.IV = IV;
                aesAlg.Padding = PaddingMode.PKCS7;
                //aesAlg.KeySize = 256;
                //byte[] output = aesAlg.DecryptCbc(cipherText, IV);
                //plaintext = Encoding.UTF8.GetString(output);
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                // Create a memory stream to hold the decrypted data
                using (var decryptedStream = new System.IO.MemoryStream())
                {
                    // Create a CryptoStream to perform the decryption
                    using (var cryptoStream = new CryptoStream(decryptedStream, decryptor, CryptoStreamMode.Write))
                    {
                        // Write the encrypted text to the CryptoStream
                        cryptoStream.Write(cipherText, 0, cipherText.Length);
                        cryptoStream.FlushFinalBlock();

                        // Convert the decrypted data to a string
                        plaintext = Encoding.UTF8.GetString(decryptedStream.ToArray());

                        //Console.WriteLine("Decrypted Text: " + decryptedText);
                    }
                }

            }
            return plaintext;
        }
        public static byte[] StringToByteArray(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        public static string Encrypt(string plainText, EncryptKey encryptKey)
        {
            byte[] IV = Encoding.UTF8.GetBytes(encryptKey.IVkey);
            byte[] Key = Encoding.UTF8.GetBytes(encryptKey.Secretkey);
            byte[] encrypted = Encrypt(plainText, Key, IV);
            return BitConverter.ToString(encrypted).Replace("-", "");
        }
        public static string Decrypt(string plainText, EncryptKey encryptKey)
        {
            byte[] IV = Encoding.UTF8.GetBytes(encryptKey.IVkey);
            byte[] Key = Encoding.UTF8.GetBytes(encryptKey.Secretkey);
            byte[] cipherText = Convert.FromHexString(plainText);
            return Decrypt(cipherText, Key, IV);
            //return BitConverter.ToString(encrypted).Replace("-", "");
        }

        public static byte[] Encrypt(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor,
                   CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }

                }
            }
            return encrypted;
        }
        public static string BatchId(string bankCode, ApplicationDbContext context)
        {

            return string.Format(Constants.BATCHID_FORMAT, DateTime.Now.ToString("ddMMyy"), bankCode, NextBatchNo(context));
        }
        public static string ApplicationUniqueId(string BatchId, ApplicationDbContext context)
        {

            return string.Format(Constants.APPLICATIONID_FORMAT, BatchId, NextBatchNo(context));
        }
        public static string NextBatchNo(ApplicationDbContext context)
        {
            long i = 0;
            try
            {
                var lastBatch = context.Batches.OrderByDescending(x => x.RecId).FirstOrDefault();
                if (lastBatch != null)
                {
                    string batchId = lastBatch.batchId;
                    i = long.Parse(batchId.Substring(batchId.Length - 5));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return (i + 1).ToString().PadLeft(5, '0');
        }
        public static string NextApplicationNo(string BatchId, ApplicationDbContext context)
        {
            long i = 0;
            try
            {
                i = context.Applications.Where(x => x.batchId == BatchId).Select(x => x.uniqueId).Count();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return i + 1.ToString().PadLeft(5, '0');
        }
        public static long NextApplicationNoLong(string BatchId, ApplicationDbContext context)
        {
            long i = 0;
            try
            {
                i = context.Applications.Where(x => x.batchId == BatchId).Select(x => x.uniqueId).Count();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return i + 1;
        }

        //public static KCCConfig GetKCCConfig(IConfiguration config)
        //{
        //    return new KCCConfig
        //    {
        //        AuthKey = config.GetSection("KCC:authKey").Value,
        //        EncryptKey = new EncryptKey
        //        {
        //            IVkey = config.GetSection("KCC:encryptKey:IVkey").Value,
        //            Secretkey = config.GetSection("KCC:encryptKey:Secretkey").Value

        //        },
        //        BankCode = config.GetSection("KCC:BankCode").Value,
        //        EndPoint = config.GetSection("KCC:Endpoint").Value,
        //        DataFilePath = config["KCC:DataFilePath"],
        //        FinancialYear = config["KCC:FinancialYear"]
        //    };

        //}
        public static DataTable ExcelToDataTable(ApplicationDbContext context, string pFilepath, int pSheetIndex)
        {
            // --------------------------------- //
            /* REFERENCIAS:
             * NPOI.dll
             * NPOI.OOXML.dll
             * NPOI.OpenXml4Net.dll */
            // --------------------------------- //
            /* USING:
             * using NPOI.SS.UserModel;
             * using NPOI.HSSF.UserModel;
             * using NPOI.XSSF.UserModel; */
            // --------------------------------- //
            DataTable Table = null;
            try
            {
                if (System.IO.File.Exists(pFilepath))
                {

                    IWorkbook workbook = null;  //IWorkbook determina se es xls o xlsx              
                    ISheet worksheet = null;
                    string first_sheet_name = "";

                    using (FileStream FS = new FileStream(pFilepath, FileMode.Open, FileAccess.Read))
                    {
                        workbook = WorkbookFactory.Create(FS); //Abre tanto XLS como XLSX
                        worksheet = workbook.GetSheetAt(pSheetIndex); //Obtener Hoja por indice
                        
                        first_sheet_name = worksheet.SheetName;  //Obtener el nombre de la Hoja

                        var firstRow = worksheet.GetRow(pSheetIndex);

                        ValidateColumnNames(context,firstRow);

                        Table = new DataTable(first_sheet_name);
                        Table.Rows.Clear();
                        Table.Columns.Clear();

                        
                        // Leer Fila por fila desde la primera
                        for (int rowIndex = 0; rowIndex <= worksheet.LastRowNum; rowIndex++)
                        {
                            DataRow NewReg = null;
                            IRow row = worksheet.GetRow(rowIndex);
                            IRow row2 = null;
                            
                            if (row != null) //null is when the row only contains empty cells 
                            {
                                if (rowIndex > 0) NewReg = Table.NewRow();

                                //Leer cada Columna de la fila
                                foreach (ICell cell in row.Cells)
                                {
                                    
                                    object valorCell = null;
                                    string cellType = "";

                                    if (rowIndex == 0) //Asumo que la primera fila contiene los titlos:
                                    {
                                        row2 = worksheet.GetRow(rowIndex + 1); //Si es la rimera fila, obtengo tambien la segunda para saber los tipos:
                                        ICell cell2 = row2.GetCell(cell.ColumnIndex);

                                        if (cell2 != null)
                                        {
                                            switch (cell2.CellType)
                                            {
                                                case CellType.Boolean: cellType = "System.Boolean"; break;
                                                case CellType.String: cellType = "System.String"; break;
                                                case CellType.Numeric:
                                                    if (HSSFDateUtil.IsCellDateFormatted(cell2)) { cellType = "System.DateTime"; }
                                                    else { cellType = "System.Double"; }
                                                    break;
                                                case CellType.Formula:
                                                    switch (cell2.CachedFormulaResultType)
                                                    {
                                                        case CellType.Boolean: cellType = "System.Boolean"; break;
                                                        case CellType.String: cellType = "System.String"; break;
                                                        case CellType.Numeric:
                                                            if (HSSFDateUtil.IsCellDateFormatted(cell2)) { cellType = "System.DateTime"; }
                                                            else { cellType = "System.Double"; }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    cellType = "System.String"; break;
                                            }

                                            //Agregar los campos de la tabla:
                                            DataColumn codigo = new DataColumn(cell.StringCellValue.Trim(), System.Type.GetType(cellType));
                                            Table.Columns.Add(codigo);
                                        }
                                        
                                    }
                                    else
                                    {
                                        //Las demas filas son registros:
                                        switch (cell.CellType)
                                        {
                                            case CellType.Blank: valorCell = DBNull.Value; break;
                                            case CellType.Boolean: valorCell = cell.BooleanCellValue; break;
                                            case CellType.String: valorCell = cell.StringCellValue; break;
                                            case CellType.Numeric:
                                                if (HSSFDateUtil.IsCellDateFormatted(cell)) { valorCell = cell.DateCellValue; }
                                                else { valorCell = cell.NumericCellValue; }
                                                break;
                                            case CellType.Formula:
                                                switch (cell.CachedFormulaResultType)
                                                {
                                                    case CellType.Blank: valorCell = DBNull.Value; break;
                                                    case CellType.String: valorCell = cell.StringCellValue; break;
                                                    case CellType.Boolean: valorCell = cell.BooleanCellValue; break;
                                                    case CellType.Numeric:
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell)) { valorCell = cell.DateCellValue; }
                                                        else { valorCell = cell.NumericCellValue; }
                                                        break;
                                                }
                                                break;
                                            default: valorCell = cell.StringCellValue; break;
                                        }
                                        NewReg[cell.ColumnIndex] = valorCell;
                                    }
                                }
                            }
                            if (rowIndex > 0) Table.Rows.Add(NewReg);
                        }
                        Table.AcceptChanges();
                    }
                }
                else
                {
                    throw new Exception("Specified File does not exists");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Table;
        }
        public static void  ValidateColumnNames(ApplicationDbContext context, IRow firstrow)
        {
            KCCConfig cCConfig = context.KCCConfig.AsNoTracking().Include(x => x.Mapping).Include(x => x.EncryptKey).FirstOrDefault();
            PropertyInfo[] requiredColumns= cCConfig.Mapping.GetType().GetProperties();
            List<string> requiredColHeder= requiredColumns.AsEnumerable().Select(x => (string)x.GetValue(cCConfig.Mapping)).ToList();
            List<string> sheetColumns = GetColNamesInExcelSheet(firstrow);
            foreach (string requiredCol in requiredColHeder)
            {
                if (!sheetColumns.Contains(requiredCol))
                {
                    throw new ValidationException("Column name " + requiredCol + " doesn't exist in the excel file");
                }
            }

        }
        public static List<string> GetColNamesInExcelSheet(IRow row)
        {
            List<string> colNames = new List<string>();
            foreach (ICell c in row.Cells)
            {
                var colData = c.StringCellValue;
                if (!string.IsNullOrWhiteSpace(colData))
                {
                    colNames.Add(colData.ToString());
                }
            }
            return colNames;
        }
        public static DataTable ToDataTable<T>(IList<T> data)
        {
            DataTable table = new DataTable("Data");
            try
            {
                PropertyDescriptorCollection properties =
                   TypeDescriptor.GetProperties(typeof(T));

                foreach (PropertyDescriptor prop in properties)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    table.Rows.Add(row);
                }

            }
            catch (Exception)
            {

            }
            return table;

        }
        public static string[] GetArrayFromString(string str, string splitStr = ",")
        {
            if (!string.IsNullOrEmpty(str))
            {
                return str.Split(splitStr);
            }
            else
            {
                return new string[0];
            }
        }

    }

    public static class JsonFileReader
    {
        public static T Read<T>(string filePath)
        {
            string text = File.ReadAllText(filePath);
            return System.Text.Json.JsonSerializer.Deserialize<T>(text);
        }
    }
    public sealed class Sequence
    {
        private int value = 0;
        private int length = 5;

        public Sequence(int length)
        {
            this.length = length;
        }

        //public string Prefix { get; }

        public int GetNextValue()
        {
            return System.Threading.Interlocked.Increment(ref this.value);
        }

        public string GetNextNumber()
        {
            return $"{this.GetNextValue()}".PadLeft(this.length,'0');
        }
        
    }
    

}