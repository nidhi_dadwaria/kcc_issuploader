﻿using KCC_App.KCCModels;
using KCC_App.Models;
using KCC_App.Utils;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.Formula.Functions;
using System.IO;
using System.Linq;

namespace KCC_App.Data
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<EncryptKey>().HasData(new EncryptKey
            {
                EncryptKeyId = 1,
                IVkey = "GLQkQmzPcDGdoN1Y",
                Secretkey = "x197MvEKV7vfmMitJPFW79jPuBKKtZq5"
            });
            Mapping mapping = JsonFileReader.Read<Mapping>(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory) + "\\mapping.json");
            mapping.MappingId = 1L;
            modelBuilder.Entity<Mapping>().HasData(mapping);

            modelBuilder.Entity<KCCConfig>().HasData(new KCCConfig
            {
                Id = 1,
                AuthKey = "69749eef-79dc-4490-8505-dadb1e51d74d",
                EncryptKeyId = 1,
                MappingId = 1,
                FinancialYear = "2021-2022",
                BankCode = "149",
                EndPoint = "https://fasalrin.gov.in/v1/issintegration",
                DataFilePath = "",
            });
            modelBuilder.Entity<LoginModel>().HasData(new LoginModel
            {
                Id = 1,
                Username = "kcc_admin",
                Password = "Kadmin@kcc"
            });
            modelBuilder.Entity<LoginModel>().HasData(new LoginModel
            {
                Id = 2,
                Username = "kcc_user",
                Password = "Kuser@kcc"
            });
            //Batch Status
            modelBuilder.Entity<BatchStatus>().HasData(new BatchStatus
            {
                status = 0,
                Name = "Discarded"
            });
            modelBuilder.Entity<BatchStatus>().HasData(new BatchStatus
            {
                status=1,
                Name = "Pending for processing"
            });
            modelBuilder.Entity<BatchStatus>().HasData(new BatchStatus
            {
                status = 2,
                Name = "Processing"
            });
            modelBuilder.Entity<BatchStatus>().HasData(new BatchStatus
            {
                status = 3,
                Name = "Processed"
            });
            //App Status
            modelBuilder.Entity<ApplicationStatus>().HasData(new ApplicationStatus
            {
                status = 0,
                Name = "Draft"
            });
            modelBuilder.Entity<ApplicationStatus>().HasData(new ApplicationStatus
            {
                status = 1,
                Name = "Pending for approval(Submitted)"
            });
            modelBuilder.Entity<ApplicationStatus>().HasData(new ApplicationStatus
            {
                status = 2,
                Name = "Approved"
            });
            modelBuilder.Entity<ApplicationStatus>().HasData(new ApplicationStatus
            {
                status = 3,
                Name = "Rejected"
            });
            modelBuilder.Entity<ApplicationStatus>().HasData(new ApplicationStatus
            {
                status = 4,
                Name = "Deleted"
            });
            modelBuilder.Entity<ApplicationStatus>().HasData(new ApplicationStatus
            {
                status = 5,
                Name = "Review Required"
            });


        }
        
    }
    public class ApplicationDbContext : DbContext
    {
        //public DbSet<KCCBatch> KCCBatches { get; set; }

        //public DbSet<KCCApplication> KCCApplications { get; set; }

        public DbSet<Batch> Batches { get; set; }

        public DbSet<Application> Applications { get; set; }

        public DbSet<AccountDetails> AccountDetails { get; set; }

        public DbSet<JointAccountHolder> JointAccountHolder { get; set; }

        public DbSet<Activity> Activity { get; set; }

        public DbSet<ActivityRow> ActivityRow { get; set; }

        public DbSet<BasicDetails> BasicDetails { get; set; }

        public DbSet<LoanDetails> LoanDetails { get; set; }

        public DbSet<ResidentialDetails> ResidentialDetails { get; set; }

        public DbSet<BatchResponse> BatchResponse { get; set; }

        public DbSet<BatchResponseData> BatchResponseData { get; set; }

        public DbSet<BatchStatus> BatchStatuses { get; set; }

        public DbSet<Mapping> Mapping { get; set; }
        public DbSet<EncryptKey> EncryptKey { get; set; }
        public DbSet<KCCConfig> KCCConfig { get; set; }

        public DbSet<Branches> Branches { get; set; }
        public DbSet<States> States { get; set; }
        public DbSet<Districts> Districts { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DbSet<Crops> Crops { get; set; }
        public DbSet<GeneralMasters> GeneralMasters { get; set; }
        public DbSet<LoginModel> LoginModels { get; set; }

        public DbSet<ApplicationStatus> ApplicationStatuses { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
                : base(options)
        {
        }

        public ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
        
    }
}



