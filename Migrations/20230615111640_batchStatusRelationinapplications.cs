﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class batchStatusRelationinapplications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Applications_recordStatus",
                table: "Applications",
                column: "recordStatus");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_BatchStatuses_recordStatus",
                table: "Applications",
                column: "recordStatus",
                principalTable: "BatchStatuses",
                principalColumn: "status",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_BatchStatuses_recordStatus",
                table: "Applications");

            migrationBuilder.DropIndex(
                name: "IX_Applications_recordStatus",
                table: "Applications");
        }
    }
}
