﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class ConfigTableSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "EncryptKey",
                columns: new[] { "EncryptKeyId", "IVkey", "Secretkey" },
                values: new object[] { 1L, "GLQkQmzPcDGdoN1Y", "x197MvEKV7vfmMitJPFW79jPuBKKtZq5" });

            migrationBuilder.InsertData(
                table: "Mapping",
                columns: new[] { "MappingId", "aadhaarNumber", "accountHolder", "accountNumber", "activityType", "beneficiaryName", "beneficiaryPassbookName", "branchCode", "cropCode", "cropName", "dob", "farmerCategory", "farmerType", "gender", "ifsc", "inlandType", "joint_aadhaarNumber", "joint_isPrimary", "joint_name", "kccDrawingLimitForFY", "kccLimitSanctionedAmount", "kccLoanSanctionedDate", "khataNumber", "landArea", "landType", "landVillage", "landVillageName", "liveStockCode", "liveStockType", "loanSanctionedAmount", "loanSanctionedDate", "marineType", "mobile", "plantationArea", "plantationCode", "primaryOccupation", "relativeName", "relativeType", "residentialAddress", "residentialPincode", "residentialVillage", "season", "socialCategory", "surveyNumber", "totalArea", "totalUnits", "unitCount" },
                values: new object[] { 1L, "ADHAR NO.(2)", "A/C HOLDER TYPE SINGLE-1,JOINT-2(19)", "LOAN AC NO.(16)", "ACTIVITY TYPE AGRI -1,HORTI&VEG CROP-2,ANIMAL HUSBANDRY-3.INLAND FISHERIES-4,MARINE-FISHEREIES-5(23)", "FARMER NAME(1)", "beneficiary passbook name(3)", "BRANCH CODE AS PER KCC ISS PORTAL(17)", "CROP CODE(27)", null, "DATE OF BIRTH(5)", "FARMER CATEGORY(OWNER-1,TENANT-3,SHARECOOPER-2)(8)", "FARMER TYPE SMALL-1,MARGINAL-2,OTHERS-3(9)", "GENDER(male-1,female-2,other-3)(6)", "IFSC(18)", "INLAND TYPE(38)", "JOINT A/C HOLDER ADHAR(43)", "JOINT A/C HOLDER TYPE PRIMARY-1,SECONDARY-0(44)", "JOINT A/C HOLDER NAME(42)", "kccDrawingLimitforFY(22)", "LAON SANCTION OR RENEW AMOUNT(21)", "LOAN SANCTION OR RENEW DATE(20)", "KHATA NO.(29)", "AREA (HCTR)(30)", "LAND TYPE (IRRIGATED-1,NON IRRIGATED-2)(31)", "LAND VILLAGE CODE(26)", null, "LIVE STOCK CODE(36)", "LIVE STOCK TYPE(35)", "LOAN SANCTION OR RENEWEL AMOUNT(25)", "LOAN SANCTION DATE(24)", "MARINE TYPE(41)", "MOB NO.(4)", "PLANTATION AREA(34)", "PLANTATION CODE(33)", "PRIMARY OCCUPATION FARMER-1,FISHRIES-2,ANIMAL- HUSBANDRY-3(10)", "RELATIVE NAME(12)", "RELATIVE TYPE(S/O-1,D/O-2,C/O-3,W/O-4)(11)", "ADDRESS(14)", "PIN CODE(15)", "VILLAGE NAME(13)", "SEASON(KHARIF-1,RABI-2)(32)", "SOCIAL CATEGORY(GEN-1,OBC-2,ST-3,SC-4)(7)", "SURVEY NO.(28)", "TOTAL AREA(40)", "TOTAL UNIT(39)", "UNIT CODE(37)" });

            migrationBuilder.InsertData(
                table: "KCCConfig",
                columns: new[] { "Id", "AuthKey", "BankCode", "DataFilePath", "EncryptKeyId", "EndPoint", "FinancialYear", "MappingId" },
                values: new object[] { 1L, "69749eef-79dc-4490-8505-dadb1e51d74d", "149", "", 1L, "https://fasalrin.gov.in/v1/issintegration", "2021-2022", 1L });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "KCCConfig",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "EncryptKey",
                keyColumn: "EncryptKeyId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Mapping",
                keyColumn: "MappingId",
                keyValue: 1L);
        }
    }
}
