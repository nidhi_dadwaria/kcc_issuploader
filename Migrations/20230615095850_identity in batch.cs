﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace KCC_App.Migrations
{
    public partial class identityinbatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "data",
                table: "BatchResponse");

            migrationBuilder.AddColumn<long>(
                name: "RecId",
                table: "Batches",
                nullable: false,
                defaultValue: 0L)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RecId",
                table: "Batches");

            migrationBuilder.AddColumn<string>(
                name: "data",
                table: "BatchResponse",
                type: "text",
                nullable: true);
        }
    }
}
