﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace KCC_App.Migrations
{
    public partial class CompleteKCCModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    accountNumber = table.Column<string>(nullable: true),
                    ifsc = table.Column<string>(nullable: true),
                    branchCode = table.Column<string>(nullable: true),
                    accountHolder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Batches",
                columns: table => new
                {
                    batchId = table.Column<string>(nullable: false),
                    financialYear = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Batches", x => x.batchId);
                });

            migrationBuilder.CreateTable(
                name: "JointAccountHolder",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccountDetailId = table.Column<long>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    aadhaarNumber = table.Column<string>(nullable: true),
                    isPrimary = table.Column<int>(nullable: false),
                    AccountDetailsId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JointAccountHolder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JointAccountHolder_AccountDetails_AccountDetailsId",
                        column: x => x.AccountDetailsId,
                        principalTable: "AccountDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    uniqueId = table.Column<string>(nullable: false),
                    batchId = table.Column<string>(nullable: true),
                    recordStatus = table.Column<int>(nullable: false),
                    accountDetailsId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.uniqueId);
                    table.ForeignKey(
                        name: "FK_Applications_AccountDetails_accountDetailsId",
                        column: x => x.accountDetailsId,
                        principalTable: "AccountDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Applications_Batches_batchId",
                        column: x => x.batchId,
                        principalTable: "Batches",
                        principalColumn: "batchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BatchResponse",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    batchId = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false),
                    error = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchResponse", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BatchResponse_Batches_batchId",
                        column: x => x.batchId,
                        principalTable: "Batches",
                        principalColumn: "batchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    activityType = table.Column<int>(nullable: false),
                    loanSanctionedDate = table.Column<string>(nullable: true),
                    loanSanctionedAmount = table.Column<int>(nullable: false),
                    ApplicationuniqueId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Activity_Applications_ApplicationuniqueId",
                        column: x => x.ApplicationuniqueId,
                        principalTable: "Applications",
                        principalColumn: "uniqueId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BasicDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ApplicationId = table.Column<string>(nullable: true),
                    beneficiaryName = table.Column<string>(nullable: true),
                    aadhaarNumber = table.Column<string>(nullable: true),
                    beneficiaryPassbookName = table.Column<string>(nullable: true),
                    mobile = table.Column<string>(nullable: true),
                    dob = table.Column<string>(nullable: true),
                    gender = table.Column<int>(nullable: false),
                    socialCategory = table.Column<int>(nullable: false),
                    farmerCategory = table.Column<int>(nullable: false),
                    farmerType = table.Column<int>(nullable: false),
                    primaryOccupation = table.Column<int>(nullable: false),
                    relativeType = table.Column<int>(nullable: false),
                    relativeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BasicDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BasicDetails_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "uniqueId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LoanDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ApplicationId = table.Column<string>(nullable: true),
                    kccLoanSanctionedDate = table.Column<string>(nullable: true),
                    kccLimitSanctionedAmount = table.Column<int>(nullable: false),
                    kccDrawingLimitForFY = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LoanDetails_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "uniqueId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ResidentialDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ApplicationId = table.Column<string>(nullable: true),
                    residentialVillage = table.Column<string>(nullable: true),
                    residentialAddress = table.Column<string>(nullable: true),
                    residentialPincode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResidentialDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResidentialDetails_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Applications",
                        principalColumn: "uniqueId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActivityRow",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ActivityId = table.Column<long>(nullable: false),
                    landVillage = table.Column<string>(nullable: true),
                    cropCode = table.Column<string>(nullable: true),
                    surveyNumber = table.Column<string>(nullable: true),
                    khataNumber = table.Column<string>(nullable: true),
                    landArea = table.Column<double>(nullable: false),
                    landType = table.Column<int>(nullable: false),
                    season = table.Column<int>(nullable: false),
                    plantationCode = table.Column<string>(nullable: true),
                    plantationArea = table.Column<int>(nullable: true),
                    liveStockType = table.Column<int>(nullable: true),
                    liveStockCode = table.Column<int>(nullable: true),
                    unitCount = table.Column<int>(nullable: true),
                    inlandType = table.Column<int>(nullable: true),
                    totalUnits = table.Column<int>(nullable: true),
                    totalArea = table.Column<int>(nullable: true),
                    marineType = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityRow", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityRow_Activity_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activity_ApplicationuniqueId",
                table: "Activity",
                column: "ApplicationuniqueId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityRow_ActivityId",
                table: "ActivityRow",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_accountDetailsId",
                table: "Applications",
                column: "accountDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_batchId",
                table: "Applications",
                column: "batchId");

            migrationBuilder.CreateIndex(
                name: "IX_BasicDetails_ApplicationId",
                table: "BasicDetails",
                column: "ApplicationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BatchResponse_batchId",
                table: "BatchResponse",
                column: "batchId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_JointAccountHolder_AccountDetailsId",
                table: "JointAccountHolder",
                column: "AccountDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanDetails_ApplicationId",
                table: "LoanDetails",
                column: "ApplicationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ResidentialDetails_ApplicationId",
                table: "ResidentialDetails",
                column: "ApplicationId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityRow");

            migrationBuilder.DropTable(
                name: "BasicDetails");

            migrationBuilder.DropTable(
                name: "BatchResponse");

            migrationBuilder.DropTable(
                name: "JointAccountHolder");

            migrationBuilder.DropTable(
                name: "LoanDetails");

            migrationBuilder.DropTable(
                name: "ResidentialDetails");

            migrationBuilder.DropTable(
                name: "Activity");

            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "AccountDetails");

            migrationBuilder.DropTable(
                name: "Batches");
        }
    }
}
