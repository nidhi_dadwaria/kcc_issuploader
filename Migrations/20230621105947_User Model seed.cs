﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class UserModelseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "LoginModels",
                columns: new[] { "Id", "Password", "Username" },
                values: new object[,]
                {
                    { 1, "Kadmin@kcc", "kcc_admin" },
                    { 2, "Kuser@kcc", "kcc_user" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "LoginModels",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "LoginModels",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
