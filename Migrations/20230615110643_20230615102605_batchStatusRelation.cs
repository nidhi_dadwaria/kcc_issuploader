﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class _20230615102605_batchStatusRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "status",
                table: "BatchResponseData",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BatchResponseData_status",
                table: "BatchResponseData",
                column: "status");

            migrationBuilder.AddForeignKey(
                name: "FK_BatchResponseData_BatchStatuses_status",
                table: "BatchResponseData",
                column: "status",
                principalTable: "BatchStatuses",
                principalColumn: "status",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BatchResponseData_BatchStatuses_status",
                table: "BatchResponseData");

            migrationBuilder.DropIndex(
                name: "IX_BatchResponseData_status",
                table: "BatchResponseData");

            migrationBuilder.DropColumn(
                name: "status",
                table: "BatchResponseData");
        }
    }
}
