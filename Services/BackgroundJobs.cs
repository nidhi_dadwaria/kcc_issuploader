﻿using KCC_App.Data;
using KCC_App.KCCModels;
using KCC_App.Models;
using KCC_App.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
//using MobileBackend.AutomailerModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KCC_App.Models;
using System.ComponentModel;
using DocumentFormat.OpenXml.InkML;
using Mapping = KCC_App.KCCModels.Mapping;
using Hangfire;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Net.Http;
using System.Text;
using System.Net.Http.Json;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Net;
using System.Text.Json.Nodes;
using KCC_App.Interfaces;

namespace KCC_App.Services
{
    public class BackgroundJobs: IBackgroundJobs
    {
        readonly KCCConfig cCConfig;
        readonly ApplicationDbContext dbContext;
        private readonly IConfiguration _configuration;
        public BackgroundJobs(IConfiguration configuration, ApplicationDbContext context)
        {
            this._configuration = configuration;
            //this.cCConfig = Common.GetKCCConfig(this._configuration);// JsonConvert.DeserializeObject<KCCConfig>(configuration.GetSection("KCC").Value);
            this.dbContext = context;
            this.cCConfig = dbContext.KCCConfig.AsNoTracking().Include(x => x.Mapping).Include(x => x.EncryptKey).FirstOrDefault();
        }
        //public async Task<Batch> FileUpload(UploadModel model)
        //{
        //    cCConfig.Mapping = JsonFileReader.Read<Mapping>(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory) + "\\mapping.json");
        //    long lastAppSeq = Common.NextApplicationNoLong(model.batchId, dbContext);
        //    List<Application> applications = new List<Application>();

        //    foreach (DataRow dataRow in model.dt.Rows)
        //    {
        //        if (dataRow[cCConfig.Mapping.beneficiaryName] != DBNull.Value)
        //        {
        //            List<Activity> activities = new List<Activity>();
        //            List<ActivityRow> activityRow = new List<ActivityRow>();
        //            #region activityrows

        //            if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 1)
        //            {

        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    cropCode = dataRow[cCConfig.Mapping.cropCode].ToString(),
        //                    surveyNumber = dataRow[cCConfig.Mapping.surveyNumber].ToString(),
        //                    khataNumber = dataRow[cCConfig.Mapping.khataNumber].ToString(),
        //                    landArea = Convert.ToDouble(dataRow[cCConfig.Mapping.landArea]),
        //                    landType = Convert.ToInt32(dataRow[cCConfig.Mapping.landType]),
        //                    season = Convert.ToInt32(dataRow[cCConfig.Mapping.season])
        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 2)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    plantationCode = dataRow[cCConfig.Mapping.plantationCode].ToString(),
        //                    surveyNumber = dataRow[cCConfig.Mapping.surveyNumber].ToString(),
        //                    khataNumber = dataRow[cCConfig.Mapping.khataNumber].ToString(),
        //                    plantationArea = Convert.ToDouble(dataRow[cCConfig.Mapping.plantationArea])

        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 3)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    liveStockType = Convert.ToInt32(dataRow[cCConfig.Mapping.liveStockType]),
        //                    liveStockCode = Convert.ToInt32(dataRow[cCConfig.Mapping.liveStockCode]),
        //                    unitCount = Convert.ToInt32(dataRow[cCConfig.Mapping.unitCount])
        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 4)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    inlandType = Convert.ToInt32(dataRow[cCConfig.Mapping.inlandType]),
        //                    totalUnits = Convert.ToInt32(dataRow[cCConfig.Mapping.totalUnits]),
        //                    totalArea = Convert.ToDouble(dataRow[cCConfig.Mapping.totalArea])
        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 5)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    marineType = Convert.ToInt32(dataRow[cCConfig.Mapping.marineType]),
        //                    totalUnits = Convert.ToInt32(dataRow[cCConfig.Mapping.totalUnits])
        //                });
        //            }
        //            #endregion
        //            activities.Add(new Activity
        //            {
        //                activityType = Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]),
        //                loanSanctionedDate = Common.KCCDate(dataRow[cCConfig.Mapping.loanSanctionedDate].ToString()),
        //                loanSanctionedAmount = Convert.ToInt32(dataRow[cCConfig.Mapping.loanSanctionedAmount]),
        //                activityRows = activityRow
        //            });
        //            Application app = new Application
        //            {
        //                uniqueId = model.batchId + (lastAppSeq.ToString().PadLeft(5, '0')),
        //                recordStatus = 1,
        //                basicDetails = new BasicDetails
        //                {
        //                    beneficiaryName = dataRow[cCConfig.Mapping.beneficiaryName].ToString(),
        //                    aadhaarNumber = dataRow[cCConfig.Mapping.aadhaarNumber].ToString(),
        //                    beneficiaryPassbookName = dataRow[cCConfig.Mapping.beneficiaryPassbookName].ToString(),
        //                    mobile = dataRow[cCConfig.Mapping.mobile].ToString(),
        //                    dob = Common.KCCDate(dataRow[cCConfig.Mapping.dob].ToString()),//Date Format yyyy-MM-dd
        //                    gender = Convert.ToInt32(dataRow[cCConfig.Mapping.gender]),
        //                    socialCategory = Convert.ToInt32(dataRow[cCConfig.Mapping.socialCategory]),
        //                    farmerCategory = Convert.ToInt32(dataRow[cCConfig.Mapping.farmerCategory]),
        //                    farmerType = Convert.ToInt32(dataRow[cCConfig.Mapping.farmerType]),
        //                    primaryOccupation = Convert.ToInt32(dataRow[cCConfig.Mapping.primaryOccupation]),
        //                    relativeType = Convert.ToInt32(dataRow[cCConfig.Mapping.relativeType]),
        //                    relativeName = dataRow[cCConfig.Mapping.relativeName].ToString()
        //                },
        //                residentialDetails = new ResidentialDetails
        //                {
        //                    residentialVillage = dataRow[cCConfig.Mapping.residentialVillage].ToString(),
        //                    residentialAddress = dataRow[cCConfig.Mapping.residentialAddress].ToString(),
        //                    residentialPincode = dataRow[cCConfig.Mapping.residentialPincode].ToString(),

        //                },
        //                accountDetails = new AccountDetails
        //                {
        //                    accountNumber = dataRow[cCConfig.Mapping.accountNumber].ToString(),
        //                    branchCode = dataRow[cCConfig.Mapping.branchCode].ToString(),
        //                    ifsc = dataRow[cCConfig.Mapping.ifsc].ToString(),
        //                    accountHolder = Convert.ToInt32(dataRow[cCConfig.Mapping.accountHolder]),
        //                    jointAccountHolders = new List<JointAccountHolder>()

        //                },
        //                loanDetails = new LoanDetails
        //                {
        //                    kccLoanSanctionedDate = Common.KCCDate(dataRow[cCConfig.Mapping.kccLoanSanctionedDate].ToString()),
        //                    kccLimitSanctionedAmount = Convert.ToInt32(dataRow[cCConfig.Mapping.kccLimitSanctionedAmount]),
        //                    kccDrawingLimitForFY = Convert.ToInt32(dataRow[cCConfig.Mapping.kccDrawingLimitForFY])
        //                }

        //            };
        //            if (app.accountDetails.accountHolder == 2)
        //            {
        //                app.accountDetails.jointAccountHolders.Add(new JointAccountHolder
        //                {
        //                    name = dataRow[cCConfig.Mapping.joint_name].ToString(),
        //                    aadhaarNumber = dataRow[cCConfig.Mapping.joint_aadhaarNumber].ToString(),
        //                    isPrimary = Convert.ToInt32(dataRow[cCConfig.Mapping.joint_isPrimary])
        //                });
        //            }

        //            applications.Add(app);
        //            lastAppSeq++;
        //        }
        //    }
        //    Batch newBatch = new Batch
        //    {
        //        batchId = model.batchId,
        //        financialYear = cCConfig.FinancialYear,

        //        applications = applications

        //    };
        //    string edata = Common.Encrypt(JsonConvert.SerializeObject(newBatch), cCConfig.EncryptKey);

        //    var body = new KCCBody
        //    {
        //        authCode = cCConfig.AuthKey,
        //        data = edata
        //    };

        //    var client = new RestClient();
        //    var request = new RestRequest(cCConfig.EndPoint);
        //    request.AddHeader("Content-Type", "application/json");
        //    request.AddBody(JsonConvert.SerializeObject(body), "application/json");
        //    var response = await client.ExecutePostAsync(request);
        //    try
        //    {
        //        BatchResponseModel batchReponseModel = JsonConvert.DeserializeObject<BatchResponseModel>(response.Content);
        //        newBatch.BatchResponse = new BatchResponse
        //        {
        //            status = batchReponseModel.status,
        //            error = batchReponseModel.error,
        //            batchId = model.batchId

        //        };
        //        if (batchReponseModel.status)
        //        {
        //            newBatch.BatchResponseData = JsonConvert.DeserializeObject<BatchResponseData>(Common.Decrypt((string)batchReponseModel.data, cCConfig.EncryptKey));
        //        }
        //        else
        //        {
        //            newBatch.BatchResponseData = new BatchResponseData
        //            {
        //                batchId = model.batchId,
        //                //status = 0
        //            };

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        newBatch.BatchResponse = new BatchResponse
        //        {
        //            batchId = model.batchId,
        //            status = false,
        //            error = ex.Message
        //        };
        //        newBatch.BatchResponseData = new BatchResponseData
        //        {
        //            batchId = model.batchId,
        //            //status = 0
        //        };
        //    }

        //    newBatch.filepath = model.filePath;
        //    newBatch.dateTime = DateTime.Now;
        //    dbContext.Batches.Add(newBatch);
        //    dbContext.SaveChanges();
        //    return newBatch;
        //}
        //public async Task KCCUpload()
        //{
        //    //read the file at the specified location and upload it to. KCC server.
        //    string _filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
        //    cCConfig.Mapping = JsonFileReader.Read<Mapping>(_filePath + "\\mapping.json");
        //    DataTable dataTable = Common.ExcelToDataTable(cCConfig.DataFilePath, 0);
        //    string batchId = Common.BatchId(cCConfig.BankCode, dbContext);
        //    //KCCBatch batch = new KCCBatch
        //    //{
        //    //    BatchId = batchId
        //    //};
        //    long lastAppSeq = Common.NextApplicationNoLong(batchId, dbContext);
        //    List<Application> applications = new List<Application>();
        //    //List<KCCApplication> kCCApplications = new List<KCCApplication>();
        //    foreach (DataRow dataRow in dataTable.Rows)
        //    {
        //        if (dataRow[cCConfig.Mapping.beneficiaryName] != DBNull.Value)
        //        {
        //            List<Activity> activities = new List<Activity>();
        //            List<ActivityRow> activityRow = new List<ActivityRow>();
        //            #region activityrows
        //            /// ACTIVITY TYPE AGRI -1,HORTI&VEG CROP-2,ANIMAL HUSBANDRY-3.INLAND FISHERIES-4,MARINE-FISHEREIES-5(23)
        //            if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 1)
        //            {

        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    cropCode = dataRow[cCConfig.Mapping.cropCode].ToString(),
        //                    surveyNumber = dataRow[cCConfig.Mapping.surveyNumber].ToString(),
        //                    khataNumber = dataRow[cCConfig.Mapping.khataNumber].ToString(),
        //                    landArea = Convert.ToDouble(dataRow[cCConfig.Mapping.landArea]),
        //                    landType = Convert.ToInt32(dataRow[cCConfig.Mapping.landType]),
        //                    season = Convert.ToInt32(dataRow[cCConfig.Mapping.season])
        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 2)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    plantationCode = dataRow[cCConfig.Mapping.plantationCode].ToString(),
        //                    surveyNumber = dataRow[cCConfig.Mapping.surveyNumber].ToString(),
        //                    khataNumber = dataRow[cCConfig.Mapping.khataNumber].ToString(),
        //                    plantationArea = Convert.ToDouble(dataRow[cCConfig.Mapping.plantationArea])

        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 3)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    liveStockType = Convert.ToInt32(dataRow[cCConfig.Mapping.liveStockType]),
        //                    liveStockCode = Convert.ToInt32(dataRow[cCConfig.Mapping.liveStockCode]),
        //                    unitCount = Convert.ToInt32(dataRow[cCConfig.Mapping.unitCount])
        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 4)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    inlandType = Convert.ToInt32(dataRow[cCConfig.Mapping.inlandType]),
        //                    totalUnits = Convert.ToInt32(dataRow[cCConfig.Mapping.totalUnits]),
        //                    totalArea = Convert.ToDouble(dataRow[cCConfig.Mapping.totalArea])
        //                });
        //            }
        //            else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 5)
        //            {
        //                activityRow.Add(new ActivityRow
        //                {
        //                    landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
        //                    marineType = Convert.ToInt32(dataRow[cCConfig.Mapping.marineType]),
        //                    totalUnits = Convert.ToInt32(dataRow[cCConfig.Mapping.totalUnits])
        //                });
        //            }
        //            #endregion
        //            activities.Add(new Activity
        //            {
        //                activityType = Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]),
        //                loanSanctionedDate = Common.KCCDate(dataRow[cCConfig.Mapping.loanSanctionedDate].ToString()),
        //                loanSanctionedAmount = Convert.ToInt32(dataRow[cCConfig.Mapping.loanSanctionedAmount]),
        //                activityRows = activityRow
        //            });
        //            Application app = new Application
        //            {
        //                uniqueId = batchId + (lastAppSeq.ToString().PadLeft(5, '0')),
        //                recordStatus = 1,
        //                basicDetails = new BasicDetails
        //                {
        //                    beneficiaryName = dataRow[cCConfig.Mapping.beneficiaryName].ToString(),
        //                    aadhaarNumber = dataRow[cCConfig.Mapping.aadhaarNumber].ToString(),
        //                    beneficiaryPassbookName = dataRow[cCConfig.Mapping.beneficiaryPassbookName].ToString(),
        //                    mobile = dataRow[cCConfig.Mapping.mobile].ToString(),
        //                    dob = Common.KCCDate(dataRow[cCConfig.Mapping.dob].ToString()),//Date Format yyyy-MM-dd
        //                    gender = Convert.ToInt32(dataRow[cCConfig.Mapping.gender]),
        //                    socialCategory = Convert.ToInt32(dataRow[cCConfig.Mapping.socialCategory]),
        //                    farmerCategory = Convert.ToInt32(dataRow[cCConfig.Mapping.farmerCategory]),
        //                    farmerType = Convert.ToInt32(dataRow[cCConfig.Mapping.farmerType]),
        //                    primaryOccupation = Convert.ToInt32(dataRow[cCConfig.Mapping.primaryOccupation]),
        //                    relativeType = Convert.ToInt32(dataRow[cCConfig.Mapping.relativeType]),
        //                    relativeName = dataRow[cCConfig.Mapping.relativeName].ToString()
        //                },
        //                residentialDetails = new ResidentialDetails
        //                {
        //                    residentialVillage = dataRow[cCConfig.Mapping.residentialVillage].ToString(),
        //                    residentialAddress = dataRow[cCConfig.Mapping.residentialAddress].ToString(),
        //                    residentialPincode = dataRow[cCConfig.Mapping.residentialPincode].ToString(),

        //                },
        //                accountDetails = new AccountDetails
        //                {
        //                    accountNumber = dataRow[cCConfig.Mapping.accountNumber].ToString(),
        //                    branchCode = dataRow[cCConfig.Mapping.branchCode].ToString(),
        //                    ifsc = dataRow[cCConfig.Mapping.ifsc].ToString(),
        //                    accountHolder = Convert.ToInt32(dataRow[cCConfig.Mapping.accountHolder]),
        //                    jointAccountHolders = new List<JointAccountHolder>()

        //                },
        //                loanDetails = new LoanDetails
        //                {
        //                    kccLoanSanctionedDate = Common.KCCDate(dataRow[cCConfig.Mapping.kccLoanSanctionedDate].ToString()),
        //                    kccLimitSanctionedAmount = Convert.ToInt32(dataRow[cCConfig.Mapping.kccLimitSanctionedAmount]),
        //                    kccDrawingLimitForFY = Convert.ToInt32(dataRow[cCConfig.Mapping.kccDrawingLimitForFY])
        //                },
        //                activities = activities

        //            };
        //            if (app.accountDetails.accountHolder == 2)
        //            {
        //                app.accountDetails.jointAccountHolders.Add(new JointAccountHolder
        //                {
        //                    name = dataRow[cCConfig.Mapping.joint_name].ToString(),
        //                    aadhaarNumber = dataRow[cCConfig.Mapping.joint_aadhaarNumber].ToString(),
        //                    isPrimary = Convert.ToInt32(dataRow[cCConfig.Mapping.joint_isPrimary])
        //                });
        //            }

        //            applications.Add(app);
        //            //kCCApplications.Add(new KCCApplication
        //            //{
        //            //    KCCBatch = batch,
        //            //    UniqueId = app.uniqueId
        //            //});
        //            lastAppSeq++;
        //        }
        //    }

        //    //this._configuration.GetSection("KCC:authKey").Value,JsonConvert.DeserializeObject<EncryptKey>(_configuration.GetSection("KCC:encryptKey").Value)
        //    //string authKey = Common.Encrypt(cCConfig.AuthKey,cCConfig.EncryptKey);


        //    Batch newBatch = new Batch
        //    {
        //        batchId = batchId,
        //        financialYear = cCConfig.FinancialYear,
        //        applications = applications

        //    };
        //    string edata = Common.Encrypt(JsonConvert.SerializeObject(newBatch), cCConfig.EncryptKey);

        //    var body = new KCCBody
        //    {
        //        authCode = cCConfig.AuthKey,
        //        data = edata
        //    };
        //    var client = new RestClient();
        //    var request = new RestRequest(cCConfig.EndPoint);
        //    request.AddHeader("Content-Type", "application/json");
        //    request.AddBody(JsonConvert.SerializeObject(body), "application/json");
        //    var response = await client.ExecutePostAsync(request);
        //    try
        //    {
        //        BatchResponse batchResponse = JsonConvert.DeserializeObject<BatchResponse>(response.Content);
        //        newBatch.BatchResponse = batchResponse;
        //    }
        //    catch (Exception ex)
        //    {
        //        newBatch.BatchResponse = new BatchResponse
        //        {
        //            error = ex.Message
        //        };
        //    }
        //    dbContext.Batches.Add(newBatch);
        //    dbContext.SaveChanges();
        //    if (!string.IsNullOrEmpty(newBatch.BatchResponse.error))
        //    {
        //        throw new Exception(response.Content.ToString());
        //    }

        //    //if (batchResponse.status && string.IsNullOrEmpty(batchResponse.error))
        //    //{

        //    //    //dbContext.KCCBatches.Add(batch);
        //    //    //dbContext.KCCApplications.AddRange(kCCApplications);
        //    //    //dbContext.SaveChanges();
        //    //    //dbContext.Batches.Add(newBatch);
        //    //    //dbContext.SaveChanges();
        //    //}
        //}


        public async Task RefreshBatchStatus(string batchAckId, string batchId)
        {
            APIModel model = new APIModel
            {
                batchAckId = batchAckId
            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(model), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint+Constants.API_ROUTE_BATCHSTATUS);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));
                    BatchResponseData batchResponseData = dbContext.BatchResponseData.FirstOrDefault(x => x.batchId == responseModel.dataD.batchId);
                    batchResponseData.status = responseModel.dataD.status;
                    batchResponseData.errors = responseModel.dataD.errors;
                    //batchResponseData.Application.applicationNumber = responseModel.dataD.applications.First().applicationNumber;
                    //batchResponseData.Application.farmerId = responseModel.dataD.applications.First().farmerId;
                    await dbContext.SaveChangesAsync();
                    if (batchResponseData.status == 3)
                    {
                        BackgroundJob.Enqueue(() => RefreshBatchAndApplicationStatus(batchAckId, batchId));
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }
        public async Task RefreshBatchAndApplicationStatus(string batchAckId, string batchId)
        {
            APIModel model = new APIModel
            {
                batchAckId = batchAckId

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(model), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint+Constants.API_ROUTE_BATCHAPPLICATIONSTATUS);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));
                    BatchResponseData batchResponseData = dbContext.BatchResponseData.FirstOrDefault(x => x.batchId == responseModel.dataD.batchId);
                    batchResponseData.status = responseModel.dataD.status;
                    batchResponseData.errors = responseModel.dataD.errors;
                    
                    var applications = dbContext.Applications.Where(x => x.batchId == batchId);
                    foreach (Application app in responseModel.dataD.applications)
                    {
                        var foundApp = applications.FirstOrDefault(x => x.uniqueId == app.uniqueId);
                        if (foundApp != null)
                        {
                            foundApp.applicationNumber = app.applicationNumber;
                            foundApp.farmerId = app.farmerId;
                            foundApp.recipientUniqueID = app.recipientUniqueID;
                            foundApp.applicationStatus = app.applicationStatus;
                            foundApp.errors = Common.GetArrayFromString((string)app.errorsJ,";");
                        }

                    }
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

            }

        }

        public async Task RefreshBatchStatus()
        {
            var unProcessedBatches = dbContext.Batches.Include(x=>x.BatchResponseData).Where(x => x.BatchResponseData.status == 1 || x.BatchResponseData.status == 2).ToList();
            foreach(Batch batch in unProcessedBatches)
            {
                BackgroundJob.Enqueue(() => RefreshBatchStatus(batch.BatchResponseData.batchAckId, batch.batchId));
            }
        }



    }
}

